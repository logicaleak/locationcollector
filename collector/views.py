from django.views.generic import View
import json
from .models import *
from django.http import HttpResponse
import datetime

# Create your views here.
class location_collector_view(View):
    def post(self, request):
        json_request = json.loads(request.body)

        unique_id = json_request['uniqueId']
        lat = json_request['lat']
        lng = json_request['lng']

        now = datetime.datetime.now()
        location = Location.objects.create(lat=lat, lng=lng, unique_id=unique_id, create_time=now)
        location.save()

        return HttpResponse()


