from django.db import models
import datetime

# Create your models here.
class Location(models.Model):
    unique_id = models.CharField(max_length=100)
    lat = models.CharField(max_length=25)
    lng = models.CharField(max_length=25)
    create_time = models.DateTimeField(default=datetime.datetime.now())
